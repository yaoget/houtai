<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
//生成验证码
function create_smscode($length=''){
    $min=pow(10,($length-1));
    $max=pow(10,$length)-1;
    return rand($min,$max);
}
//验证手机号是否注册
function check_register($phone=''){
    $rs=Db()->where('phone',$phone)->find();
    if($rs){
        return true;
    }else{
        return false;
    }
}
//验证手机号
function check_mobile($phone=''){
    if (preg_match("/1[34578]{1}\d{9}$/", $phone)) {
        return true;
    } else {
        return false;
    }
}
/**
 * 返回
 */
function ajax_success($data=''){
    $data=[
        'code'=>101,
        'data'=>$data
    ];
    return $data;
}
function ajax_error($info=''){
    $data=[
        'code'=>100,
        'data'=>[
            'info'=>$info
        ]
        ];
        return $data;
}
function ajax_syserror($info=''){
    $data=[
        'code'=>96,
        'data'=>[
            'info'=>$info
        ]
        ];
        return $data;
}
/**
 * 生成token
 */
function get_token(){
    $charid = strtoupper(md5(uniqid(mt_rand(), true)));
    return substr($charid, 0, 8) . substr($charid, 8, 4) . substr($charid, 12, 4) . substr($charid, 16, 4) . substr($charid, 20, 12);
}
/**
 * 验证token
 */
function check_token($token='',$userid=''){
    $rs=Db('user')->where(['token'=>$token,'id'=>$userid])->find();
    if($rs and $rs['last_time']+604800>time()){
        return true;
    }else{
        return false;
    }
}
/**
 * 发送短信验证码
 */
function sendmsg($mobilephone='',$content=''){
    $config= Config('msg');
        $ssender = new \sms\SmsSingleSender($config['appid'], $config['appkey']);
        $result = $ssender->sendWithParam("86", $mobilephone, $config['templateId'],
        $content, '', "", "");
        //$rsp = json_decode($result);
        return $result;
}
/**
 * 获取用户真实ip
 */
function get_ip(){
    //判断服务器是否允许$_SERVER
    if(isset($_SERVER)){    
        if(isset($_SERVER[HTTP_X_FORWARDED_FOR])){
            $realip = $_SERVER[HTTP_X_FORWARDED_FOR];
        }elseif(isset($_SERVER[HTTP_CLIENT_IP])) {
            $realip = $_SERVER[HTTP_CLIENT_IP];
        }else{
            $realip = $_SERVER[REMOTE_ADDR];
        }
    }else{
        //不允许就使用getenv获取  
        if(getenv("HTTP_X_FORWARDED_FOR")){
              $realip = getenv( "HTTP_X_FORWARDED_FOR");
        }elseif(getenv("HTTP_CLIENT_IP")) {
              $realip = getenv("HTTP_CLIENT_IP");
        }else{
              $realip = getenv("REMOTE_ADDR");
        }
    }

    return $realip;
}
/**
 * 判断是否是好友，或者已经发送好友请求
 */
function check_friend($user_id='',$friend_id=''){
    $rs=Db('friend')->where(['user_id'=>$user_id,'friend_id'=>$friend_id])->find();
    if($rs){
        return $rs['status'];
    }else{
        return '-2';
    }

}
function get_game($user_id,$game_id,$data){
    $check_mate=Db('room')->where(['user_id|friend'=>$user_id,'game_id'=>$game_id,'status'=>1])->find();
    if($check_mate){
        if($user_id=$check_mate['user_id']){
            $result=[
                'user_id'=>$user_id,
                'friend'=>$check_mate['friend'],
                'room_id'=>$roomid
            ];
        }else{
            $result=[
                'user_id'=>$check_mate['friend'],
                'friend'=>$user_id,
                'room_id'=>$roomid
            ];
        }
        return $result;
    }
    foreach($data as $k=>$v){
        if($v==$game_id and $k!=$user_id){
            $roomid=Db('room')->insertGetId(['game_id'=>$game_id,'user_id'=>$user_id,'friend'=>$k,'status'=>1]);
            $result=[
                'user_id'=>$user_id,
                'friend'=>$k,
                'room_id'=>$roomid
            ];
            break;
        }
    }
    if(isset($result)){
        return $result;
    }else{
        return false;
    }
}