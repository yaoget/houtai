<?php 
namespace app\admins\controller;
class Room extends BaseAdmin{
    public function index(){
        $data=[];
        $data = input('post.');
        if(empty($data['user_id'])){
            unset($data['user_id']);
        }
        if(empty($data['status'])){
            if(!$data['status'] == 0){
                unset($data['status']);
            }
        }
        $list=Db('room')->where($data)->select();
        $this->assign('list',$list);
        return $this->fetch();
    
}

    /*删除*/
    public function room_del(){

        $id = (int)input('post.id');

        if(request()->isPost()){
           
            $res = Db('room')->where('id',$id)->delete();

            if($res){
                return $this->success('删除成功');
            }else{
              return $this->error('删除失败');
            }
        }
    } 
}

 