<?php
namespace app\admins\controller;
class Device extends BaseAdmin{
    public function index(){
      /*显示所有设备*/
      $list=Db('device')->paginate(20);
      $this->assign('list',$list);
      return $this->fetch();
    }
    
    public function device_add(){
        $id = (int)input('get.id');

        $res = Db('device')->where('id',$id)->find();
        $this->assign('data',$res);
       
        return $this->fetch();
    }

    public function device_save(){
        
        $data = input('post.');
  
        $id = $data['id'];
        $data['create_time'] = time();
        if($data['id']>0){
            $res = Db('device')->where('id',$id)->update($data);
        }else{
            $res = Db('device')->insert($data);
        }
        if($res){
            return $this->success('操作成功');
        }else{
            return $this->error('操作失败');
        }
        
    }


    public function device_del(){
      
       $id = (int)input('post.id');
       $res = Db('device')->where('id',$id)->delete();

       exit(json_encode(array('code'=>0,'msg'=>'删除成功')));


    }
}