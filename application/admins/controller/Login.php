<?php
namespace app\admins\controller;
use think\Controller;
class Login extends Controller{
    public function index(){
        return $this->fetch();
    }
    public function dologin(){
        if(request()->isPost()){
            $data=input('post.');
            foreach($data as $v){
                if($v==''){
                    return $this->error('请输入用户名或密码');
                }
            }
            $password=md5($data['password']);
            $rs=Db('admin')->where(['username'=>$data['username'],'password'=>$password])->find();
            if($rs){
                unset($rs['password']);
                session('admin', $rs);
                return json(['code'=>0,'msg'=>'登录成功']);
            }else{
                return json(['code'=>1,'msg'=>'用户名或密码错误']);
            }
        }
        return null;
    }

}