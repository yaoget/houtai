<?php
namespace app\admins\controller;
use think\Controller;
   /*禁止非法用户登陆控制器*/
 class BaseAdmin extendS Controller{

     public function __construct(){
         parent::__construct();
         $this->_admin = session('admin');
         if(!$this->_admin){
             return $this->redirect("login/index");
         }
     }
}
