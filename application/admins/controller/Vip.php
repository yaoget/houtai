<?php
namespace app\admins\controller;
class Vip extends BaseAdmin{
    public function index(){
        $list=Db('user')->paginate(20);
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function feedback(){
        $list=Db('feedback')->paginate(20);
        $this->assign('list',$list);
        return $this->fetch();
    }
}