<?php
namespace app\admins\controller;
class Game extends BaseAdmin{
    public function index(){
        $list=Db('game')->paginate('20');
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function add(){
        $data='';
        $pics = '';
        $id = input('get.id');
       
        $res = Db('game')->where('id',$id)->find();
        if($res){
        $pics = explode('|',$res['pics']);    
    }
        $this->assign('list',$pics);
        $this->assign('data',$res);
       
        return $this->fetch();

      
    }
    public function game_cate(){
        $list=Db('cate')->paginate(20);
        $this->assign('list',$list);
        return $this->fetch();
    }
    /*单文件*/ 
    public function upload(){
        $file = request()->file('file');
        if($file==null){
            exit(json_encode(array('code'=>0,'msg'=>'没有文件上传')));
        }
        $info = $file->move(ROOT_PATH.'public'.DS.'uploads');
        $ext = ($info->getExtension());
        if(!in_array($ext,array('jpg','jpeg','gif','png'))){
            exit(json_encode(array('code'=>1,'msg'=>'文件格式不支持')));
        }
        $imgurl = '/uploads/'.$info->getSaveName();
        exit(json_encode(array('code'=>1,'msg'=>$imgurl)));
    }
   
    public function cate_add(){
        $id = (int)input('get.id');

        $res = Db('cate')->where('id',$id)->find();
        $this->assign('data',$res);
       
        return $this->fetch();
    }
    public function save_cate(){
        if($_POST){
            $data=input('post.');
            $data['create_time']=time();
            if(empty($data['id'])){
                $rs=Db('cate')->insert($data);
                if($rs){
                    return $this->success('添加成功');
                }else{
                    return $this->error('添加失败');
                }
            }else{
                $rs=Db('cate')->where('id',$data['id'])->update($data);
                if($rs){
                    return $this->success('修改成功');
                }else{
                    return $this->error('修改失败');
                }
            }
        }
        return null;
    }
     

    /*保存游戏*/
    public function game_save(){

        
        if($_POST){
            $data = input('post.');
            $data['create_time'] = time();
            $pics = input('post.pics/a');
            $data['pics'] = implode('|',$pics);
            if(request()->isPost()){
                if($data['id']>0){
                   $res  =  Db('game')->where('id',$data['id'])->update($data);
                }else{
                    $res = Db('game')->insert($data);
                }     
            }
            
            if($res){
                return $this->success('操作成功');
            }else{
                return $this->error('操作失败');
            }
            }
        }
      /*删除游戏*/ 
       public function game_del(){
           if($_POST){
               $id = (int)input('post.id');
               $res = Db('game')->where('id',$id)->delete();
               if($res){
                return $this->success('删除成功');
              }else{
                return $this->error('删除失败');
              }
           }
       }

    }
