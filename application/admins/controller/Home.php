<?php
namespace app\admins\controller;
class Home extends BaseAdmin{
    public function index(){
        return $this->fetch();
    }
    public function changepasswd(){
        if(request()->isPost()){
            $data=input('post.');
            $password=Db('admin')->where('username',session('admin.username'))->value('password');
            if($password != md5($data['oldpassword'])){
                return $this->error('旧密码错误');
            }
            if($data['newpassword'] != $data['confirmpassword']){
                return $this->error('两次密码输入不一致');
            }
            $rs=Db('admin')->where('username',session('admin.username'))->update(['password'=>md5($data['newpassword'])]);
            if($rs){
                return $this->success('修改成功');
            }else{
                return $this->error('修改失败');
            }
        }
        return $this->fetch();
    }
    public function msg(){
        $data=[];
        if($_POST){
            $data=input('post.');
            if(empty($data['mobile'])){
                unset($data['mobile']);
            }
            if(empty($data['status'])){
                unset($data['status']);
            }
        }
        $list=Db('sms')->where($data)->paginate(20);
        $this->assign('list',$list);
        return $this->fetch();
    }

}