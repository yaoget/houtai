<?php
namespace app\index\controller;
class User extends Base{
    protected $game;
    /**
     * 提交反馈
     */
    public function submit_feedback(){
        if(request()->isPost()){
            $feedinfo=input('post.');
            $data=[
                'user_id'=>$feedinfo['user_id'],
                'content'=>$feedinfo['content'],
                'create_time'=>time(),
                'mobile'=>$feedinfo['mobile']
            ];
            $rs=Db('feedback')->insert($data);
            if($rs){
                return json(ajax_success());
            }else{
                return json(ajax_error('提交失败'));
            }  
        }
        return null;
    }
    /**
     * 上传图片
     */
    public function upload(){
        $file = request()->file('file');
        $id=input('post.id');
        if($file==null){
            return json(ajax_error('没有图片上传'));
        }
        $info = $file->validate(['ext'=>'jpg,png,gif,jpeg'])->move(ROOT_PATH . 'public' . DS . 'uploads'.DS.'image');
        if($info){
            echo $info->getSaveName(),$id;
        }else{
            echo $file->getError();
        }
    }
    /**
     * 获取好友列表
     */
    public function get_friend(){
        if(request()->isPost()){
            $userid=input('post.id');
            $data=Db('friend')->where('userid',$userid)->select();
            if($data){
                return json(ajax_success($data));
            }else{
                return json(ajax_error('拉取好友列表失败'));
            }
        }
    }
    /**
     * 邀请好友参加游戏
     */
    public function invite_friend(){
        if(request()->isPost()){
            $data=input('post.');

        }
    }
    /**
     * 添加好友
     */
    public function add_friend(){
       // if(request()->isPost()){
            $data=input('post.');
            $data=[
                'user_id'=>1,
                'friend'=>10
            ];
            $rs=Db('user')->where('id|mobile',$data['friend'])->find();
            if(!$rs){
                return json(ajax_error('用户不存在'));
            }
            $check=check_friend($data['user_id'],$rs['id']);
            if($check>=0){
                switch($check){
                    case 0:
                    $rdata='已发送请求';
                    break;
                    case 1:
                    $rdata='已经是好友了';
                    break;
                }
                return json(ajax_success($rdata));
            }
            if($check== -1){
                $result=Db('friend')->where(['user_id'=>$data['user_id'],'friend_id'=>$rs['id']])->update(['status'=>0,'create_time'=>time()]);
            }else{
                $result=Db('friend')->insert(['user_id'=>$data['user_id'],'friend_id'=>$rs['id'],'status'=>0,'create_time'=>time()]);
            }
            if($result){
                return json(ajax_success('发送请求成功'));
            }else{
                return json(ajax_error('发送请求失败'));
            }
        //}
    }
    /**
     * 匹配好友
     */
    public function mate_friend(){
        //if(request()->isPost()){
            $data=input('post.');
            $data['user_id']=422;
            $data['game_id']=5;
            $rs=Db('room')->where(['game_id'=>$data['game_id'],'status'=>0,'user_id'=>['<>',$data['user_id']]])->find();
            if($rs){
                Db('room')->where('id',$rs['id'])->update(['friend'=>$data['user_id']]);
                $rdata=[
                    'friend'=>$rs['user_id'],
                    'room_id'=>$rs['id']
                ];
                return json(ajax_success($rdata));
            }else{
                Db('room')->insert(['user_id'=>$data['user_id'],'game_id'=>$data['game_id'],'status'=>0]);
                return json(ajax_error('等待匹配'));
            }
            
      //  }
    }
    public function query_mate(){
       // if(request()->isPost()){
            $data=input('post.');
            $data['user_id']=422;
            $data['game_id']=5;
            $rs=Db('room')->where(['game_id'=>$data['game_id'],'status'=>0,'user_id'=>['<>',$data['user_id']]])->find();
            if($rs){
                Db('room')->where('id',$rs['id'])->update(['friend'=>$data['user_id'],'status'=>1]);
                $rdata=[
                    'friend'=>$rs['user_id'],
                    'room_id'=>$rs['id']
                ];
                return json(ajax_success($rdata));
            }else{
                return json(ajax_error('等待匹配'));
            }


      //  }

    }
}