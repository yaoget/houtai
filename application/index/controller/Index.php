<?php
namespace app\index\controller;

class Index
{
    /**
     * 获取游戏分类
     */
    public function get_cate(){
        $rs=Db('cate')->select();
        if($rs){
            return json(ajax_success($rs));
        }else{
            return json(ajax_error('获取游戏分类失败'));
        }
    }
    /**
     * 获取游戏列表
     */
    public function get_gamelist(){
        $data=input('post.');
        // $data['cate_id']=1;
        // $data['keywords']='你猜';
        if(!empty($data['cate_id']) and !empty($data['keywords'])){
            $list=Db('game')->where(['cate_id'=>$data['cate_id'],'name'=>['like','%'.$data['keywords'].'%']])->select();
            if($list){
                return json(ajax_success($list));
            }else{
                return json(ajax_error('没有了'));
            }
        }else{
            if(!empty($data['cate_id'])){
                $list=Db('game')->where('cate_id',$data['cate_id'])->select();
                return json(ajax_success($list));
            }
            if(!empty($data['keywords'])){
                $list=Db('game')->where('name','like','%'.$data['keywords'].'%')->select();
                return json(ajax_success($list));
            }
        }
        return json(ajax_syserror('缺少参数'));
        

    }
    /**
     * 获取游戏详情
     */
    public function get_gamedetail(){
        if(request()->isPost()){
            $game_id=input('post.game_id');
            $data=Db('game')->where('id',$game_id)->find();
            if($data){
                return json(ajax_success($data));
            }else{
                return json(ajax_error('获取游戏详情失败'));
            }
        }
   return null;
    }
}
