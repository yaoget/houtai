<?php
namespace app\index\controller;
class Login{
    public function login(){
        if(request()->isPost()){
            $data=input('post.');
            if($data['mobile'] !='' and $data['code'] !=''){
                //验证短信验证码
                $rs=Db('sms_code')->where('mobile',$data['mobile'])->find();
                if($rs['send_time']+600<time() and $data['code']==$rs['code']){
                    if(check_register($data['mobile'])){
                        //生成token并插入数据库,并查找用户信息
                        $token=get_token();
                        Db('user')->where('mobile',$data['mobile'])->update(['last_ip'=>get_ip(),'last_time'=>time(),'token'=>$token]);
                        return json(['code'=>100,'data'=>['token'=>$token,'userinfo'=>'userinfo']]);
                    }else{
                        //插入用户信息，并返回token
                        $token=get_token();
                        $result=Db('user')->insert(['moblie'=>$data['mobile'],'reg_time'=>time(),'reg_ip'=>get_ip(),'last_ip'=>get_ip(),'last_time'=>time(),'token'=>$token]);
                        $data=['token'=>$token];
                        return json(ajax_success($token));

                    }
                }else{
                    return json(ajax_error('验证码失效或错误'));
                }
            }
        }
    }
    public function register(){
        // if(request()->isPost()){
            $mobile=input('post.mobile');
            // switch($data['act']){
            //     case 'act':
            // }
            $mobile='17671301300';
            if(check_mobile($mobile)){
               //发送验证码
               $check=Db('sms_code')->where('mobile',$mobile)->find();
               $number=$check['number'];
               if($check){
                //    if($check['send_time']+60<time()){
                //     $etime=time()-$check['send_time'];
                //     return json(ajax_error('请'.$etime.'秒后重试'));
                //    }
                   $time=date('m')*100+date('d');
                   $rtime=date('m',$check['send_time'])*100+date('d',$check['send_time']);
                   if($time==$rtime){
                       if($number>=4){
                        return json(ajax_error('超过发送验证码次数'));
                       }
                   }else{
                       Db('sms_code')->where('mobile',$mobile)->update(['number'=>0]);
                       $number=0;
                   }
               }
               $code=create_smscode(4);
               $content=array($code,'30');
               $rs=sendmsg($mobile,$content);
               $rs=json_decode($rs,true);
            if($rs['result']==0){
                Db('sms')->insert(['mobile'=>$mobile,'content'=>$code,'msg'=>'验证码','create_time'=>time(),'status'=>1]);
                
                if($check){
                    Db('sms_code')->where('mobile',$mobile)->update(['send_time'=>time(),'code'=>$code,'number'=>$number+1]);
                }else{
                    Db('sms_code')->insert(['mobile'=>$mobile,'code'=>$code,'send_time'=>time(),'number'=>$number+1]);
                }
                return json(ajax_success());
            }else{
                Db('sms')->insert(['mobile'=>$mobile,'content'=>$code,'msg'=>'验证码','create_time'=>time(),'status'=>-1]);
                return json(ajax_error($rs['errmsg']));
            }
            }else{
                return json(ajax_error('手机号格式错误'));
            }
     //  }
    }
}