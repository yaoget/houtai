var common_obj = {
    data_handle: function (callback) {
        //操作
        $('table').on("click", "td a[href=#deal]", function () {
            $this = $(this);

            if ($(this).attr("data-text")) {
                var check = confirm($(this).attr("data-text"));
                if (check == false) {
                    return false
                }
            }

            var url = $(this).attr('data-url');
            var id = $(this).parent().attr('data-pid');
            var tablename = $(this).attr('data-tablename');
            var fieldname = $(this).attr('data-fieldname');
            var afterchange = $(this).attr("data-value");
            $.post(url, {
                id: id,
                tablename: tablename,
                fieldname: fieldname,
                afterchange: afterchange
            }, function (data) {
                if (data.ret == 1) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    common_obj.load_form(callback);
                } else {
                    alert(data.msg);
                    return false;
                }
            }, 'json');
        });

        common_obj.load_form(callback);
        $('#search-form').submit(function (e) {
            e.preventDefault();
            $("#search-form input[name=page]").val(1).attr("data-value", 1);
            common_obj.load_form(callback);
        });

        //点击底部页面分页
        $("#page").on("click", ".pagination li a", function (e) {
            e.preventDefault();
            var page = $(this).attr("href");
            page = page.split('?');
            page = page[1].substring(5);

            $("#search-form input[name=page]").val(page).attr("data-value", page);
            common_obj.load_form(callback);
        });

        //导出
        $('#search-form input:button').click(function () {
            window.location = $(this).attr("data-url");
        });
    },

    load_form: function (callback) {
        $('#search-form input:not(input:submit),#search-form select').each(function () {
            $(this).attr("data-value", $(this).val());
            //console.log($(this).val());  console.log($(this).attr("data-value"));
        });
        //return false;
        var obj = {};
        $.each($('#search-form input:not(input:submit):not(input:button),#search-form select'), function (i, n) {
            var name = $(this).attr("name");
            obj[name] = $(this).attr("data-value");
        })


        $.post('', obj, function (data) {
            if (data.ret == 1) {
                $("table tbody").html(data.data);
                if ($("#page").length) {
                    $("#page").html(data.attach.page)
                }
            } else {
                if (data.msg) {
                    alert(data.msg);
                    return false;
                }
            }
            if (callback) {
                callback(data);
            }
        }, 'json');
    },

    form_submit: function () {
        //单张图片上传
        if ($("form .one-img").length) {
            $("form .one-img").each(function () {
                webuploader_obj.webuploader_init('#' + $(this).find(".webuploader-picker").attr("id"), $(this).find("input:hidden"), $(this).find(".img"));
            });
        }

        //多张图片
        if ($("form .more-img").length) {
            $(".more-img").on('click', ".img div span", function () {
                $(this).parent().remove();
            });

            $("form .more-img").each(function () {
                $this = $(this);

                $this.find('.img div span').on('click', function () {
                    $(this).parent().remove();
                });

                pic_count = parseInt($this.find(".help-block span").text());
                callback = function (imgpath, file_input_obj) {
                    var file_obj = $(file_input_obj).parents(".more-img");

                    pic_count = parseInt(file_obj.find(".help-block span").text());
                    if (file_obj.find('.img div').size() >= pic_count) {
                        alert('您上传的图片数量已经超过' + pic_count + '张，不能再上传！');
                        return false;
                    }
                    var html = '<div>' + webuploader_obj.img_link(imgpath) + '<span>删除</span><input type="hidden" name="' + file_obj.attr("data-imgname") + '" value="' + imgpath + '" /></div>';
                    file_obj.find('.img').append(html);
                };

                webuploader_obj.webuploader_init('#' + $this.find(".webuploader-picker").attr("id"), '', '', '', true, pic_count, callback);
            });
        }

        //地图
        if ($("form .map").length) {
            window.addEventListener('message', function (event) {
                // 接收位置信息，用户选择确认位置点后选点组件会触发该事件，回传用户的位置信息
                var loc = event.data;
                //防止其他应用也会向该页面post信息，需判断module是否为'locationPicker'
                if (loc && loc.module == 'locationPicker') {
                    $("input[name=lng]").val(loc.latlng.lng);
                    $("input[name=lat]").val(loc.latlng.lat);
                    $("input[name=address]").val(loc.poiaddress);
                    $(".map p.form-control-static").text(loc.poiaddress);
                }
            }, false);
        }

        $('#data-form').submit(function (e) {
            e.preventDefault();
            $.post($("#do_action").val(), $('#data-form').serialize(), function (data) {
                if (data.ret == 1) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    window.location.reload();
                } else {
                    if (data.msg) {
                        alert(data.msg);
                        return false;
                    }
                }
            }, 'json');
        });
    },
}

var webuploader_obj = {

    webuploader_init: function (file_input_obj, filepath_input_obj, img_detail_obj, size) {//file_input_obj 上传图片元素 filepath_input_obj图片路径元素 img_detail_obj 追加图片元素
        var multi = (typeof(arguments[4]) == 'undefined') ? false : arguments[4];	//是否多张
        var queueSizeLimit = (typeof(arguments[5]) == 'undefined') ? 5 : arguments[5];	//最多上传张数
        var callback = arguments[6];	//回调函数
        var fileExt = (typeof(arguments[7]) == 'undefined') ? '*.jpg;*.png;*.gif;*.jpeg;*.bmp' : arguments[7];//可上传格式
        //console.log(file_input_obj);console.log(filepath_input_obj);console.log(img_detail_obj);console.log(size);
        var uploader = WebUploader.create({
            auto: true,
            swf: './static/common/js/webuploader/Uploader.swf',// swf文件路径
            server: '/member.php/Member/webfileupload',// 文件接收服务端。

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: {
                id: file_input_obj,
                multiple: multi
            },
            resize: false,// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            disableGlobalDnd: true,	 // 禁掉全局的拖拽功能。这样不会出现图片拖进页面的时候，把图片打开。
            fileNumLimit: 300	//最多上传张数          
        });

        // 文件上传成功，给item添加成功class, 用样式标记上传成功。
        uploader.on('uploadSuccess', function (file, response) {

            //var jsonData=eval('('+response+')');
            var jsonData = response;
            if (jsonData.status == 1) {
                if (!multi) {//单图
                    filepath_input_obj.val(jsonData.imgpath);
                    img_detail_obj.html(webuploader_obj.img_link(jsonData.imgpath));
                } else {
                    callback(jsonData.imgpath, file_input_obj);
                }
            } else {
                alert('文件上传失败，出现未知错误！');
            }
        });

        // 文件上传失败，显示上传出错。
        uploader.on('uploadError', function (file, response) {
            var jsonData = eval('(' + response + ')');
            if (jsonData.status == 1) {

            } else {
                alert('文件上传失败，出现未知错误！');
            }
        });

        // 完成上传完了，成功或者失败，先删除进度条。
        uploader.on('uploadComplete', function (file, response) {

        });
    },

    img_link: function (img) {
        if (!img) {
            return;
        }
        return '<a href="' + img + '" target="_blank"><img src="' + img + '"></a>';
    }
}
